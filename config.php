<?php
// Blog settings
$BLOG_URL = ''; // The address of your blog ending by a / For example: https://example.com/
$BLOG_TITLE = 'Blog title'; // Blog title
$BLOG_TAGLINE = 'Blog tagline below the title'; // Blog tagline or leave empty
$BLOG_DESCRIPTION = 'Blog description'; // Will be shown when sharing links from your blog
$BLOG_FOOTER = 'Footer text'; // Text displayed in the footer
$LANG = 'en'; // Two-letter language code of the blog's language
$SHOW_SUMMARY = true; // Change to true if you want to only display a short summary in home
$GOAT_COUNTER = ""; // Specify your Goat Counter code to enable analytics

// Author
$BLOG_AUTHOR = 'Name'; // Author's name or nickname
$AUTHOR_EMAIL = 'Email'; // Author's email
$AUTHOR_ABOUT = 'Byline'; // Author info box displayed at the bottom of the post. Upload an author.png to the /img directory

// Content directories (do not change)
$CONTENT_DIR = 'content';
$ARTICLES_DIR = $CONTENT_DIR . DIRECTORY_SEPARATOR . 'articles';
$ARTICLES_DRAFTS_DIR = $ARTICLES_DIR . DIRECTORY_SEPARATOR . 'drafts';
$ARTICLES_IMAGES_DIR = $ARTICLES_DIR . DIRECTORY_SEPARATOR . 'img';
$PAGES_DIR = $CONTENT_DIR . DIRECTORY_SEPARATOR . 'pages';
$PAGES_DRAFTS_DIR = $PAGES_DIR . DIRECTORY_SEPARATOR . 'drafts';
$PAGES_IMAGES_DIR = $PAGES_DIR . DIRECTORY_SEPARATOR . 'img';
