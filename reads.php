<?php include 'inc/header.php';


// Article reads
$article_reads_count = 0;
// Get all .reads files
$all_reads_articles = glob('reads' . DIRECTORY_SEPARATOR . '*.reads');
// Read value from each .reads file and add it $article_reads_count
foreach ($all_reads_articles  as $reads_file) {
    // Check if the corresponding .md file exists
    if (file_exists($ARTICLES_DIR . DIRECTORY_SEPARATOR . pathinfo($reads_file, PATHINFO_FILENAME) . '.md')) {
        $article_reads_count += fgets(fopen($reads_file, 'r'));
    } else {
        // if the corresponding .md file doesn't exist
        // delete the .reads file and remove it from
        // the $all_reads_articles array
        unlink($reads_file);
        $value = array_search($reads_file, $all_reads_articles);
        unset($all_reads_articles[$value]);
    }
}

// Pages reads
$pages_reads_count = 0;
// Get all .reads files
$all_reads_pages = glob('reads/pages' . DIRECTORY_SEPARATOR . '*.reads');

// Read value from each .reads file and add it $pages_reads_count
foreach ($all_reads_pages  as $reads_file) {
    // Check if the corresponding .md file exists
    if (file_exists($PAGES_DIR . DIRECTORY_SEPARATOR . pathinfo($reads_file, PATHINFO_FILENAME) . '.md')) {
        $pages_reads_count += fgets(fopen($reads_file, 'r'));
    } else {
        // if the corresponding .md file doesn't exist
        // delete the .reads file and remove it from
        // the $all_reads_pages array
        unlink($reads_file);
        $value = array_search($reads_file, $all_reads_pages);
        unset($all_reads_pages[$value]);
    }
}

echo '<h2>' . L::reads . '</h2>';
echo '<table style="font-family: Inter; letter-spacing: 1px;">';
echo '<tr><td>' . L::reads_articles . '</td><td style="text-align: right; font-weight: bold;">' . $article_reads_count . '</td></tr>';
echo '<tr><td>' . L::reads_pages . '</td><td style="text-align: right; font-weight: bold;">' . $pages_reads_count . '</td></tr>';
echo '<tr><td>' . L::reads_total . '</td><td style="text-align: right; font-weight: bold;">' . $article_reads_count + $pages_reads_count . '</td></tr>';
echo '</table>';
echo '<hr>';

$reads_array = array();
// Read value from each .reads file and add it $count
foreach ($all_reads_articles  as $reads_file) {
    $count = fgets(fopen($reads_file, 'r'));
    $title = pathinfo($reads_file, PATHINFO_FILENAME);
    $reads_array[$title] = $count;
}
arsort($reads_array);
echo '<table style="width: 100%;">';
foreach ($reads_array as $key => $value) {
    $line = ltrim(fgets(fopen($ARTICLES_DIR .DIRECTORY_SEPARATOR . $key . '.md', 'r')), '# ');
    echo "<tr><td style='text-align: left;'><a href='article.php?id=" . $key . "'>" . $line . "</a></td><td style='padding-left: 1em;'>$value</td></tr>";
}
echo '</table>';
echo '<hr>';

$reads_array = array();
// Read value from each .download file and add it $count
foreach ($all_reads_pages  as $reads_file) {
    $count = fgets(fopen($reads_file, 'r'));
    $title = pathinfo($reads_file, PATHINFO_FILENAME);
    $reads_array[$title] = $count;
}
arsort($reads_array);
echo '<table style="width: 100%;">';
foreach ($reads_array as $key => $value) {
    $line = ltrim(fgets(fopen($PAGES_DIR .DIRECTORY_SEPARATOR . $key . '.md', 'r')), '# ');
    echo "<tr><td style='text-align: left;'><a href='page.php?id=" . $key . "'>" . $line . "</a></td><td style='padding-left: 1em;'>$value</td></tr>";
}
echo '</table>';
?>

<div style="text-align:center; margin-bottom: 2em; margin-top: 2em;"><a href="<?php echo $BLOG_URL; ?>" class="btn"><?php echo L::back_btn; ?></a></div>

<?php include 'inc/footer.php'; ?>