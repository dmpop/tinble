FROM docker.io/alpine:latest
LABEL maintainer="me@dmpop.xyz"
LABEL version="1.0"
LABEL description="Tinble container image"
RUN apk update
RUN apk add php-cli php-gd php-xml php-session
COPY . /usr/src/tinble
WORKDIR /usr/src/tinble
EXPOSE 8000
CMD [ "php", "-S", "0.0.0.0:8000" ]
