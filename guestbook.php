<?php
include('config.php');
session_start();
include 'inc/header.php';

// If the form was submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check the captcha
    if ($_SESSION["captcha"] != $_POST["captcha"]) {
        $error_msg = L::gb_error_msg;
    } else {
        // Get the form data
        $name = $_POST['name'] ? $_POST['name'] : L::gb_default_user;
        $email = $_POST['email'] ? $_POST['email'] : NULL;
        $message = $_POST['message'];
        if (!empty($message)) {
            // Load the existing data
            $data = array();
            if (file_exists('guestbook-entries.json') && filesize('guestbook-entries.json') > 0) {
                $data = json_decode(file_get_contents('guestbook-entries.json'), true);
            }
            // Check if the message already exists
            $flag = true;
            foreach ($data as $submission) {
                if ($submission['name'] == $name && $submission['message'] == $message) {
                    $flag = false;
                    break;
                }
            }
            // If the message is new, add it to the data
            if ($flag) {
                array_unshift($data, array("name" => $name, "email" => $email, "message" => $message));
                file_put_contents('guestbook-entries.json', json_encode($data, JSON_PRETTY_PRINT));
            }
        }
    }
}
// Load the data
if (!file_exists('guestbook-entries.json')) {
    touch('guestbook-entries.json');
} else {
    $json = file_get_contents('guestbook-entries.json');
    $data = json_decode($json, true);
}
?>

<div class="card" style="margin-top:2em;">
    <h1 style="text-align: center;"><?php echo L::gb_header; ?></h1>
    <hr>
    <form autocomplete="off" action="" method="POST">
        <label for="name"><?php echo L::gb_name; ?></label>
        <input class="w-100" type="text" id="name" name="name" placeholder="<?php echo L::gb_placeholder_user; ?>">
        <label for="email"><?php echo L::gb_email; ?></label>
        <input class="w-100" type="text" id="email" name="email" placeholder="<?php echo L::gb_placeholder_email; ?>">
        <label for="message"><?php echo L::gb_message; ?></label>
        <textarea class="w-100" id="message" name="message"></textarea>
        <img src="captcha.php" alt="captcha">
        <input style="vertical-align: middle;" class="w-50" type="text" name="captcha" placeholder="<?php echo L::gb_placeholder_captcha; ?>">
        <div style="margin-top: 1em;">
            <input class="btn" style="margin-right: 0.5em;" type="submit" name="submit" value="<?php echo L::gb_submit_btn; ?>">
            <a href="<?php echo $BLOG_URL; ?>" class="btn back"><?php echo L::back_btn; ?></a>
        </div>
    </form>
</div>

<?php if (isset($error_msg)) : ?>
    <h3><?= $error_msg ?></h3>
<?php endif; ?>

<div class="card" style="margin-top:2em;">
    <h1 style="text-align: center; margin-bottom: 1.5em;"><?php echo L::gb_guestbook; ?></h1>
    <hr>
    <?php
    if (!empty($data)) {
        foreach ($data as $item) {
            echo '<div style="margin-top: 1em;"><span style="font-weight: bold; text-decoration: underline;">' . htmlspecialchars($item['name']) . '</span>: ' . htmlspecialchars($item['message']) . '</div>';
        }
    } else {
        echo "<h3 style='text-align: center;'>¯\_(⊙_ʖ⊙)_/¯</h3>";
    }
    ?>
</div>

<div style="text-align:center; margin-bottom: 2.5em; margin-top: 2em;"><a href="<?php echo $BLOG_URL; ?>" class="btn"><?php echo L::back_btn; ?></a></div>

<?php
include 'inc/footer.php';
?>