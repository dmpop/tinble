# Tinble

Tinble is a simple lightweight blog engine that uses Markdown-formatted text files. It started its life as a heavily modified fork of [Tiny Blog Engine](https://github.com/nithou/tiny-blog-engine). Tinble stands for **Tin**y **bl**og **e**ngine.

Tinble is very much old-school. Apart from a tiny bit of JavaScript used for more convenient navigation, the entire application is written in PHP by a clueless but curious human.

## Features and functionality

- **Lightweight** Only a handful of PHP and CSS files, plus a single optional JavaScript library.
- **Straightforward deployment** Copy all the files to the document root of your web server with PHP.
- **Syntax highlighting** using the [Rainbow](https://craig.is/making/rainbows) library.
- **Dark theme** It's there for those who care about it.
- **Scheduled article publishing** Articles in the _drafts_ directory are automatically published on the dates specified in the file name prefixes (for example, the _2030-07-18\_first-article.md_ file in the _drafts_ directory is published automatically on July 18, 2030).
- **Pinned articles** You can pin articles to the top by adding _pinned_ anywhere in their file names.
- **Simple guestbook** Instead of comments, Tinble comes with an old-school guestbook.
- **Search** The search feature can be used to find articles containing the specified search term.
- **Basic stats** Number of reads for each article and page. Total number of reads.
- **RSS2 support** Tinble automatically generates RRS feed.
- **i18n support** The application can be easily localized to any language.
- **Self-contained and GDPR-compliant** No external dependencies, no cookies, no trackers. All required libraries and fonts are included.

## Requirements

- A web server with PHP version 7.x or higher. Tested with Apache
- PHP GD PHP extension
- PXP XML extension (only if you plan to use HTML in Markdown files)

## Installation

1. Checkout the latest code using Git:

   ```
   git clone https://codeberg.org/dmpop/tinble.git
   ```

   Alternatively, download the source code from the project's [Codeberg repository](https://codeberg.org/dmpop/tinble), and unpack the downloaded archive.

2. Move all the files in the _tinble_ directory, to the document root of your server.
3. Make the _content_ directory writable by the server. For example: `chown www-data:www-data -R /var/www/html/content`.

## Run Tinble in a container

Perform the following steps on the machine you want to use as a Tinble server.

1. Install [Docker](https://docker.com).
2. Run the `hostname -I` command and note the IP address of the machine.
3. Create a directory for storing content.
4. Clone the Tinble Git repository using the `git clone https://codeberg.org/dmpop/tinble.git` command.
5. Switch to the resulting _tinble_ directory, open the _config.php_ file for editing and replace the default value of the `$BLOG_URL` to the IP address of the machine.
6. Build an image using the `docker build -t tinble .` command.
7. Run a container: `docker run -d --rm -p 8000:8000 --name=tinble -v /path/to/content:/usr/src/tinble/content:rw tinble` (replace _/path/to/content_ with the actual path to the created directory).
5. Point the browser to _http://127.0.0.1:8000_ (replace _127.0.0.1:8000_ with the actual IP address or domain name of the machine running the container).

### Deploy Tinble with docker-compose.yml

Using the supplied _docker-compose.yml_ file, you can deploy Tinble on a machine with a domain name assigned to it. This approach automatically enables HTTPS.

1. Open the _Caddyfile_ for editing.
2. Replace `<email address>` with the email address you want to use with the Let's Encrypt service.
3. Replace `<domain name>` with the domain name assigned to the server.
4. Save the changes.
5. Run the `docker compose -d`command.

## Adding articles

To add an article, create a _.md_ file in the _articles_ directory using the following naming rule: `yyyy-mm-dd_title.md` (for example, `1970-01-01_this-is-first-article.md`).

To insert an image into an article, put the image into the _articles_ directory and use the following Markdown code:

```
![This is a nice image](articles/image.jpg)
```

When uploading an image for use in an article, resize the image and reduce its quality. Alternatively, you can use the included _tim.php_ script (requires the GD library), as follows:

```
![This is a nice image](../tim.php?image=content/articles/img/image.jpg)
```

The script resizes the inserted image to 800px on-the-fly.

## Scheduling articles

Place the article you want to publish on a specific date in the _articles/drafts_ folder, and use the desired date as the filename prefix. For example, the article named _2030-07-18\_first-article.md_ will be automatically published on July 18, 2030.

## Adding pages

To add a page, create a _.md_ file in the _pages_ directory.

## Disabling read count

To prevent Tinble from counting your own reads, append the `noreads` parameter to the blog's URL, for example: _https://domain.tld/?noreads_ or _https://domain.tld/index.php?noreads_. You have to do this once. When the read count is disabled, the **Reads** button is colored red. To re-enable the read count, delete the `noreads` cookie.

# License

[GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)

# Acknowledgements

- [Tiny Blog Engine](https://github.com/nithou/tiny-blog-engine)
- [Guestbook](https://github.com/ArticExploit/guestbook)
- [php-i18n](https://github.com/Philipp15b/php-i18n)