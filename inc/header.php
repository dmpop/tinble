<?php

// Include i18n class and initialize it
require_once 'lib/i18n.class.php';
$i18n = new i18n();
$i18n->setCachePath('cache');
$i18n->setFilePath('lang/{LANGUAGE}.ini');
$i18n->setFallbackLang('en');
$i18n->init();

include('config.php');

// Create the required directories if they don't exist
if (!file_exists($ARTICLES_DIR)) {
    mkdir($ARTICLES_DIR, 0755, true);
}
if (!file_exists($ARTICLES_DRAFTS_DIR)) {
    mkdir($ARTICLES_DRAFTS_DIR, 0755, true);
}
if (!file_exists($ARTICLES_IMAGES_DIR)) {
    mkdir($ARTICLES_IMAGES_DIR, 0755, true);
}
if (!file_exists($PAGES_DIR)) {
    mkdir($PAGES_DIR, 0755, true);
}
if (!file_exists($PAGES_DRAFTS_DIR)) {
    mkdir($PAGES_DRAFTS_DIR, 0755, true);
}
if (!file_exists($PAGES_IMAGES_DIR)) {
    mkdir($PAGES_IMAGES_DIR, 0755, true);
}
if (!file_exists('reads')) {
    mkdir('reads', 0755, true);
}
if (!file_exists('reads/pages')) {
    mkdir('reads/pages', 0755, true);
}

// If $_GET['noreads'] is set, set a cookie to disable counting reads
if (isset($_GET['noreads'])) {
    setcookie("noreads", "0", 2147483647);
}

// Publish drafts
$drafts = glob($ARTICLES_DRAFTS_DIR . DIRECTORY_SEPARATOR . date('Y-m-d') . '*.md');

foreach ($drafts as $draft) {
    rename($draft, $ARTICLES_DIR . DIRECTORY_SEPARATOR . basename($draft));
}

// Specify title for the index.php, reads.php, and guestbook.php
if (stripos($_SERVER['REQUEST_URI'], 'reads.php')) {
    $title = $BLOG_TITLE . ' | ' . L::reads;
} elseif (stripos($_SERVER['REQUEST_URI'], 'guestbook.php')) {
    $title = $BLOG_TITLE . ' | ' . L::gb_guestbook;
} else {
    $title = $BLOG_TITLE;
}

function get_title_summary($file_path)
{
    $content = file_get_contents($file_path);
    $lines = explode("\n", $content);
    $title = substr($lines[0], 2); // Extract the title by removing preceding "#"
    $summary = implode("\n", array_slice($lines, 1, 2)); // Extract the first two lines as summary
    return ['title' => $title, 'summary' => $summary];
}

if (stripos($_SERVER['REQUEST_URI'], 'article.php')) {
    $id = $_GET['id'];
    $path = $ARTICLES_DIR . DIRECTORY_SEPARATOR . $id . '.md';
    if (file_exists($path)) {
        $title_and_summary = get_title_summary($path);
        $title = $BLOG_TITLE . ' | ' . $title_and_summary['title'];
        $summary = $title_and_summary['summary'];
    }
} elseif (stripos($_SERVER['REQUEST_URI'], 'page.php')) {
    $id = $_GET['id'];
    $path = $PAGES_DIR . DIRECTORY_SEPARATOR . $id . '.md';
    if (file_exists($path)) {
        $title_and_summary = get_title_summary($path);
        $title = $BLOG_TITLE . ' | ' . $title_and_summary['title'];
        $summary = $title_and_summary['summary'];
    }
}
?>

<!DOCTYPE html>
<html lang="<?php echo $LANG; ?>">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="robots" content="noai, noimageai">
    <meta property="og:locale" content="en_US" />
    <meta property="og:image" content="<?php echo $BLOG_URL; ?>img/favicon.png" />
    <meta name="author" content="<?php echo $BLOG_AUTHOR; ?>">
    <link rel="alternate" type="application/rss+xml" href="rss.php" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $BLOG_URL; ?>img/favicon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $BLOG_URL; ?>img/favicon.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $BLOG_URL; ?>img/favicon.png" />
    <link rel="me" href="mailto:<?php echo $AUTHOR_EMAIL; ?>" />
    <link rel="me" class="h-card u-url" href="<?php echo $BLOG_URL; ?>" />

    <?php include('lib/parsedown.php'); ?>
    <?php include('lib/ParsedownExtra.php'); ?>

    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $BLOG_DESCRIPTION; ?>" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:description" content="<?php echo $BLOG_DESCRIPTION; ?>" />
    <link rel="stylesheet" href="<?php echo $BLOG_URL; ?>css/styles.css">
    <link rel="stylesheet" href="<?php echo $BLOG_URL; ?>css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/css/rainbow.css">
    <link rel="stylesheet" type="text/css" href="/css/admonitions.css">
    <script src="/js/rainbow-custom.min.js"></script>
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
</head>

<body>
    <div class="topcorner">
        <form autocomplete="off" method="GET" action="index.php">
            <input id="query" type="text" placeholder="<?php echo L::query; ?>" name="q">
        </form>
    </div>
    <div class="c">
        <div style="text-align: center; margin-top: 2em;">
            <a href="index.php">
                <img style="display: inline; height: 5em; vertical-align: middle;" src="img/favicon.svg" alt="logo" />
                <h1 class="title"><?php echo $BLOG_TITLE; ?></h1>
            </a>
            <h3 style="text-align: center; font-family: Lora; font-weight: 100; font-style:italic; margin-bottom: 1.5em;"><?php echo $BLOG_TAGLINE; ?></h3>
            <?php $index = false; ?>
            <?php include 'menu.php';
            if (file_exists("preamble.md")) {
                $preamble = file_get_contents("preamble.md");
                $Parsedown = new ParsedownExtra();
                echo '<div style="margin-top:1.5em;"';
                echo $Parsedown->text($preamble);
                echo '</div>';
            }
            ?>
            <hr>
        </div>