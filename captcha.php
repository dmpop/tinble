<?php
// Start a session to store the CAPTCHA
session_start();

// Generate a random number between 10000 and 99999
$captcha = rand(10000, 99999);

// Store the CAPTCHA in the session
$_SESSION["captcha"] = $captcha;

// Create a new true color image
$im = imagecreatetruecolor(59, 30);

// Allocate colors for the image
$bg = imagecolorallocate($im, 166, 166, 166);
$fg = imagecolorallocate($im, 255, 255, 255);

// Fill the image with the background color
imagefill($im, 0, 0, $bg);

// Output the CAPTCHA text in the image
imagestring($im, 7, 7, 7, $captcha, $fg);

// Set headers to prevent caching and specify content type
header("Cache-Control: no-store, no-cache, must-revalidate");
header('Content-type: image/png');

// Output the image
imagepng($im);

// Free memory
imagedestroy($im);
?>
